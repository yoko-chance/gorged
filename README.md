Gorged
======
Gorgedは標準入力で受け取った文字列を画像のように扱い拡大するアプリケーションです

```sh
$ echo 1001 | gorged 4
1111000000001111
1111000000001111
1111000000001111
1111000000001111
```

Installation
------------
```sh
go install gitlab.com/yoko-chance/gorged
```
