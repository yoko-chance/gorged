package main

import (
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"

	"gitlab.com/yoko-chance/slurp"
	"golang.org/x/term"
)

func main() {
	if term.IsTerminal(0) {
		return
	}

	if len(os.Args) < 2 {
		log.Fatal("require <magnification>")
	}

	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	slurp.ReadLines(os.Stdin, makeHandler(n, []string{
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
		"k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
		"u", "v", "w", "x", "y", "z",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
		"U", "V", "W", "X", "Y", "Z",
	}))
}

func makeHandler(n int, tgts []string) slurp.Handler {
	return func(l string) error {
		extended := l

		for _, t := range tgts {
			extended = strings.Replace(extended, t, strings.Repeat(t, n), -1)
		}

		for _, s := range l {
			w := (string)(s)
			if !slices.Contains(tgts, w) {
				log.Fatal("Unexpected character")
			}
		}

		fatted := ""
		for range make([]int, n) {
			fatted += "\n" + extended
		}

		fmt.Println(strings.TrimLeft(fatted, "\n"))

		return nil
	}
}
