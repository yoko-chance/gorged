module gitlab.com/yoko-chance/gorged

go 1.22.1

require (
	gitlab.com/yoko-chance/slurp v0.2.2
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf
)

require golang.org/x/sys v0.0.0-20201214210602-f9fddec55a1e // indirect
